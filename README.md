#Purple Air Viewer

This repo contains a library to access [Purple Air](http://www.purpleair.com) air quality sensor data and Purple Air Viewer, a small application that given a location as input, finds the N closest sensors to this location and displays data recorded by them:

![Page_1](screenshots/page_1.png)
![Page_2](screenshots/page_2.png)

##The purpleair Library

The "purpleair" subdirectory in this repo contains the library to access sensor data. You can download it from the [Downloads](https://bitbucket.org/christian_czezatke/purple-air/downloads/) section of this repository and install with

     pip3 install purpleair-0.1.tar.gz

###Documentation

Documentation for the library is available [here](https://www.ceci.at/projects/purpleair/).

###purpleair.sensors
This is the actual sensor API (which at this point lacks documentation for the most part). It allows to query an inventory of all sensors and then filters can be applied (such as location/proximity) to find sensors of interest. At this point i would suggest that you use Purple Air Viewer (main.py) as sample code.

Once sensors of interest have been located, data for each sensor can be queried from both Purple Air (which hosts a summary), as well as historic data that is stored on Thingspeak for each sensor.

###purpleair.aqi
This module contains code to convert sensor readings for 2.5ug/m^3 particles to the EPA's Air Quality Index (AQI). From a sensor reading in ug/m^3 you can get the AQI index, the official label for the current air quality level (eg: "Moderate") and the color code used for that level that is consistent with the one used by the EPA.

##Main Executable "main.py":

###Requirements

#### Python3
The Purple Air Viewer is written in Python3, so you need to have Python3.

#### Plotly/Dash
Since the purpose of this project was partly to familiarize myself with the basics of the free version of [Plotly's Dash](https://plot.ly/products/dash/), you need to have the Python library for dash installed. This should be as simple as running


     pip3 install dash
     pip3 install dash-core-components
     pip3 install dash-html-components
	 
	 
The official documentation for installing dash is available [here](https://dash.plot.ly/installation).


####geopy
To resolve addresses into GPS coordinates (to locate nearby sensors), Purple Air Viewer uses [geopy](https://pypi.org/project/geopy/).  Again you can install it using pip3.


##Usage

Starting the main.py will launch a http server that is bound to port 8050. You can access the UI by pointing a web browser at that address/port. On the local host, just use:



     http://127.0.0.1:8050


Right now the values displayed on the first tab are automatically upated, while graphs shown on the second page (weekly data) are not.

###Warning:

Starting main.py will cause it to bind to all available IP addresses on your host, so unless you are running a local firewall the web page will be accessible from other hosts on the network.

