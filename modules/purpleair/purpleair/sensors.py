"""
purpleair/sensors.py
(C) 2018, Christian Czezatke

Python API for retrieving PurpleAir sensor data. This code is based on information
found in the following document:

https://github.com/bomeara/purpleairpy/blob/master/api.md
"""

from threading import Lock
import requests
import time
import json
import os
from geopy import distance

PURPLEAIR_GET_ALL_SENSORS_URL="https://www.purpleair.com/json"
PURPLEAIR_GET_SENSOR_URL="https://www.purpleair.com/json"

# Location of timezone
TIMEZONE_FILE = "/etc/timezone"


class ChannelHandle(object):
    def __init__(self, raw_data):
        #print("ChannelHandle: {0}".format(raw_data))
        self.sensor_id = raw_data["ID"]
        self.primary_id = raw_data["THINGSPEAK_PRIMARY_ID"]
        self.primary_key = raw_data["THINGSPEAK_PRIMARY_ID_READ_KEY"]
        self.secondary_id = raw_data["THINGSPEAK_SECONDARY_ID"]
        self.secondary_key = raw_data["THINGSPEAK_SECONDARY_ID_READ_KEY"]

    def to_dict(self):
        return {
            "ID": self.sensor_id,
            "THINGSPEAK_PRIMARY_ID": self.primary_id,
            "THINGSPEAK_PRIMARY_ID_READ_KEY": self.primary_key,
            "THINGSPEAK_SECONDARY_ID": self.secondary_id,
            "THINGSPEAK_SECONDARY_ID_READ_KEY": self.secondary_key,
        }
        
class SensorHandle(object):
    """
    A SensorHandle is used to retrieve either live or historic data for a given
    sensor SensorHandles can be obtained via the "get_sensors" method from
    the  (:class:`purpleair.sensors.Diretory`) class.

    TODO:
    Currently retrieval of historic data is hard-wired to only retrieve data from
    the past 7 days.
    """

    # This dictionary describes how sensor readings are mapped to ThingSpeak
    # properties. There are two separate ThingSpeak IDs for each channel (a
    # primary and a secondary one).
    #
    # For example, the PM10 particle count as reported by Channel A of
    # a sensor is mapped to the secondary Thingspeak ID for that sensor
    # and corresponds to the field named "field6" in the data stream
    # stored for the secondary ThingSpeak ID of that channel.
    KEYS = {
        "PM1.0"       : [ "CH_A", 1, "field7" ],
        "PM2.5"       : [ "CH_A", 0, "field8" ],
        "PM10"        : [ "CH_A", 1, "field6" ],
        "TEMPERATURE" : [ "CH_A", 0, "field6" ],
        "HUMIDITY"    : [ "CH_A", 0, "field7" ],
        "PRESSURE"    : [ "CH_B", 0, "field6" ],
        

    }

    
    def __init__(self, raw_data):
        """
        Construct a SensorHandle from the raw data for the sensor stored
        in the Directory. -- Note that the proper way of obtaining a SensorHandle
        is through the Directory class.
        """
        
        # Copy information for both channels.
        self.channels = dict()
        self.channels["CH_A"] = ChannelHandle(raw_data["CH_A"])

        try:
            self.channels["CH_B"] = ChannelHandle(raw_data["CH_B"])
        except KeyError:
            # Some older sensors or indoor sensors don't have a secondary ID.
            pass

        ch_a = raw_data["CH_A"]
        self.last_seen = ch_a["LastSeen"]
        self.location = (ch_a["Lat"], ch_a["Lon"])
        self.label = ch_a["Label"]

        
    def to_dict(self):
        """
        This is a helper function to produce a dictionary representing the
        SensorHandle instance that matches what we get from a Directory.

        This can be used to reconstruct a sensor handle from that dictionary
        later on. (Useful when developing web applications where state is only
        kept on the client side/the web browser, where only textual data can
        be stored.)
        """
        
        d = dict()
        for key, val in self.channels.items():
            d[key] = val.to_dict()
        ch_a = d["CH_A"]
        ch_a["LastSeen"] = self.last_seen
        ch_a["Lat"], ch_a["Lon"]  = self.location
        ch_a["Label"] = self.label
        return d

    
    def get_live_data(self, channel="CH_A"):
        """
        Get the live sensor data for a sensor's channel.
        """
        
        response = requests.get(PURPLEAIR_GET_SENSOR_URL,
            params={"show": str(self.channels[channel].sensor_id)})
        res = response.json()["results"][0]
        #print("GET LIVE {0}".format(res))
        return res

    
    def get_historic_data(self, property):
        """
        Retrieve historic sensor data from ThingSpeak.

        Args
           property (str)

           Any of the keys used in the SensorHandle.KEYS dictionary
           (PM1.0, PM2.5, PM10, TEMPERATURE, HUMIDITY, PRESSURE).

        Result
           Returns two lists:
           The first one is a list of timestamps/labels for each
           measurement taken. The second one are the actual measurements.
           Note that the second list can contain "None" entries when
           the sensor did not report data for that particular timestamp.

        TODO:
           The time period for this is currently hardcoded to the past
           seven days. There are also limitations that ThingSpeak imposes
           w.r.t. the max. number of data items returned in a single query.

           One week does not hit this limit, but querying larger time spans
           will likely hit this, and we'd have to break up queries into
           multiple requests.
        """
        
        channel_id, pri_sec, field = SensorHandle.KEYS[property]

        channel = self.channels[channel_id]
        if pri_sec == 0:
            ts_id = channel.primary_id
            ts_key = channel.primary_key
        else:
            ts_id = channel.secondary_id
            ts_key = channel.secondary_key

        try:
            with open(TIMEZONE_FILE, "r") as f:
                timezone = f.readline().strip()
        except:
            timezone = None

        url = "https://api.thingspeak.com/channels/{0}/feeds.json".format(ts_id)
        params = {
            "api_key": ts_key,
            "days": "7",
            "timezone": timezone,
            #"start": "2018-11-20%2000:00:00",
            #"end": "2018-11-21%2023:00:00",
        }

        url += "?"
        for x in params:
            url += "{0}={1}&".format(x, params[x])
        url = url[:-1]
    
        response = requests.get(url)
        data = response.json()

        if data["channel"][field] == "Unused":
            labels = numbers = None
        else:
            feeds = response.json()["feeds"]
            numbers = [float(x[field]) if x[field] is not None else None
                       for x in feeds]
            labels = [x["created_at"] for x in feeds]
        return labels, numbers
                   
        
class Directory(object):
    """
    The sensor directory is a list of all sensors known to the Purpleair service.

    Most sensors have two channels ("A" and "B") and data between these sensors has
    to be correlated in order for readings to be considered valid.

    The directory as served by Purpleair lists both channels for a sensor separately.
    We process the directory and combine multiple channels into a single sensor.

    The directory is cached for up to MAX_CACHE_TIME seconds. Sensors that have not
    reported any readings longer than RECENT_TIME_LIMIT seconds are considered offline.

    The sensor Directory can be filtered when retrieving sensors and a few filters
    are pre-defined in this class.
    """

    _refresh_lock = Lock()
    MAX_CACHE_TIME=600
    RECENT_TIME_LIMIT=3600

    @staticmethod
    def outdoors(x):
        """
        A filter function that disregards all sensors that are not flagged as
        outdoors.
        """
        
        return x["CH_A"]["DEVICE_LOCATIONTYPE"] == "outside"

    
    @staticmethod
    def recently_reporting(x, time_limit=RECENT_TIME_LIMIT):
        """
        A filter function that disregards all sensors that have not reported and
        data in the past hour.
        """

        return time.time() - x["CH_A"]["LastSeen"] <= time_limit

    
    @staticmethod
    def close_by(x, latitude, longitude, max_distance_km=None):
        """
        A sorting filter function that only passes sensors that are within a given max.
        distance from a specifiec location.
        """

        dist = distance.distance( (latitude, longitude),
                                  (x["CH_A"]["Lat"], x["CH_A"]["Lon"]) )

        match = dist.km <= max_distance_km if max_distance_km is not None else True
        return (match, dist.km)
        
    
    def __init__(self, cache_file=".purpleair_all"):
        """
        Construct a sensor Directory. Usually your program will need only a
        single instance of this, since filters do not actually modify the
        Directory itself.

        Args:
           cache_file (str)
              Path to be used for cacheing the sensor directory. Remove this file
              to force an immediate reload.
        """

        self._cache_file=cache_file
        with Directory._refresh_lock:
            self._sensor_list = self._populate()

            
    def get_sensors(self, filter_func=lambda x: True,
                    sort_filter_func=None, max_result=0):
        """
        Get a list of (:class:`purpleair.sensors.SensorHandle`) instances from
        the Directory.

        Args:
           filter_func (lambda)
              A function with one unbound argument that takes a filter entry from
              the Directory as input and returns True/False, indicating that the
              corresponding sensor passed/failed the filter. "outdoors" and
              "recently_reporting" are examples.

          sort_filter_func (lambda)
              Similar to filter_func, but when a sensor passes a Sorting filter,
              an additional argument is returned that is a key by which sensors
              can be sorted. An example is "close_by" that only passes sensors that
              are up to a maximum distance away from a given location, but also
              returns the distance from the given location as a key.

          max_result (int)
              Maximum number of sensors to return. 0 means no limitation. If a sorting
              filter is used, max_result specifies the first N results to be returned
              when matching sensors are sorted in ascending order.
        """
        
        sensor_list = [ x for x in self._sensor_list if filter_func(x)]

        result = []
        if sort_filter_func is not None:
            tagged_result = []
            for s in sensor_list:
                match, sort_key = sort_filter_func(s)
                if match:
                    tagged_result.append([sort_key, s])
            # Now sort by sort key
            tagged_result.sort(key=lambda d: d[0])
            result = [x[1] for x in tagged_result]
        else:
            result = sensor_list

        # Trim list to return only the requested number of results
        if max_result != 0 and len(result) > max_result:
            result = result[:max_result]

        return [ SensorHandle(x) for x in result]
            
                    
    def _populate(self):        
        # First we try to load sensors from our local cache file:
        data = None
        if os.path.isfile(self._cache_file):
            try:
                file_age = os.path.getmtime(self._cache_file)
                if time.time() - file_age <= MAX_CACHE_TIME:
                    # Cache the list of all stations MAX_CACHE_TIME
                    with open(cache_file, "r") as f:
                        data = json.load(f)
            except Exception as e:
                os.unlink(self._cache_file)

        if data is None:
            data = self._get_list_from_server()
            with open(self._cache_file, "w") as f:
                json.dump(data, f, indent=4, sort_keys=True)
        return data


    def _get_list_from_server(self):
        response = requests.get(PURPLEAIR_GET_ALL_SENSORS_URL)

        sensors = response.json()["results"]

        # Newer detectors (PA II) have two sensors, a primary and a secondary
        # one. The secondary sensor has a parentId, while the primary has not.
        # Let's group primaries and secondaries together.
        primaries = {}
        secondaries = []
        stacked = []

        # First, split up sensors in a dictionary of primary sensors (for
        # faster lookup) and a list of secondary sensors.
        for sensor in sensors:
            if sensor["ParentID"]:
                # We are dealing with a secondary sensor:
                secondaries.append(sensor)
            else:
                # No ParentID means we are dealing with a primary sensor:
                primaries[sensor["ID"]] = sensor

        # Now let's combind primary and secondary sensors into channel A and B.    
        key_cnt = 0
        for s in secondaries:
            try:
                stacked.append({"CH_A": primaries[s["ParentID"]], "CH_B": s})
            except KeyError as e:
                # A secondary sensor whose primary doesn't exist. -- For
                # now we ignore them.
                key_cnt += 1;

        if key_cnt > 0:
            print("Found {0} secondary sensors without a primary sensor, ignoring."\
                  .format(key_cnt))
        return stacked
