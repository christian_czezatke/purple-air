"""
purpleair/aqi.py
(C) 2018, Christian Czezatke

Helper functions to handle conversion and display of the
Air Quality Index (AQI) as defined by the EPA.

Conversion from ug/m^3 to AQI for 2.5ug/m^3 pollutants is done according to

https://forum.airnowtech.org/t/the-aqi-equation/169

Color coding for air quality indices as used on the EPA's air quality web site:
https://airnow.gov
"""

# Table for 2.5ug/m^3 limits and conversion to AQI.
#
# Columns are:
# -- Limit in ug/m^3
# -- Corresponding Air Quality Index (AQI)
# -- Name for the AQI bracket as used by the EPA
# -- Color for the AQI bracket as used on the EPA's web site,
#    suitable for html rendering
PM25_TBL = [
    [  0.0,   0, "Good",           "#00cc00"],
    [ 12.0,  50, "Good",           "#00cc00"],
    [ 35.4, 100, "Moderate",       "#ffff00"],
    [ 55.4, 150, "USG",            "#ff6600"],
    [150.4, 200, "Unhealthy",      "#ff0000"],
    [250.4, 300, "Very Unhealthy", "#99004c"],
    [500.4, 500, "Hazardous",      "#7e0023"]
]

# Symbolic names for the entries in a list from the PM25_TBL
PM25_IDX_UGM3  = 0
PM25_IDX_AQI   = 1
PM25_IDX_LABEL = 2
PM25_IDX_COLOR = 3

# Values above 500 are beyond the scope of the AQI
PM25_DEATH = [-1, -1, "DEATH",      "#ffffff"]


def pm25_to_aqi(pm25: float) -> int:
    """
    Convert particulate matter for 2.5ug/m^3 to the EPA Air qualit index.

    Parameters:
    pm25 -- Floating point value for 2.5um particulate matter concentration
            in the air as ug/m^3

    Result:
    <int> -- The corresponding air quality index.

    Note: The AQI gets capped at 500. Values over 500 would be considered
          "beyond the scope of this scale" by the EPA.
    """

    # EPA standard requires that the pm25 ug/m^3 gets truncated (not rounded)
    # to the nearest 0.1ug/m^3
    pm25 = float(int(pm25*10)/10)

    idx = -1
    for x in range(len(PM25_TBL)):
        if pm25 <= PM25_TBL[x][PM25_IDX_UGM3]:
            idx = x
            break

    if idx == -1:
        # DEATH
        result = 1000000
    else:
        slope = (PM25_TBL[idx][PM25_IDX_AQI] - PM25_TBL[idx-1][PM25_IDX_AQI])/ \
                (PM25_TBL[idx][PM25_IDX_UGM3] - PM25_TBL[idx-1][PM25_IDX_UGM3])
        pm_delta = pm25 - PM25_TBL[idx-1][PM25_IDX_UGM3]

        result = int(round(slope * pm_delta + PM25_TBL[idx-1][PM25_IDX_AQI]))
    # We cap at 500
    return min(result, PM25_TBL[-1][PM25_IDX_AQI])


def _aqi_get_row(aqi: int) -> int:
    """
    Helper function to retrieve a row from the AQI table, corresponding to
    the AQI value specified.

    Paramters
    aqi -- Air Quality Index for which to retrieve the table row.

    Result
    ubt -- Index in the PM25_TBL
    """

    idx = -1
    for x in range(len(PM25_TBL)):
        if aqi <= PM25_TBL[x][1]:
            idx = x
            break
    row = PM25_TBL[idx] if idx != -1 else PM25_DEATH
    return row


def aqi_to_color(aqi: int) -> str:
    """
    Return a HTML compliant color code for a given air quality index.

    Paramters
    aqi -- Air Quality Index for which to return a color code.

    Result:
    Color as used on the EPA web site in HTML format (eg: "#99004c").
    """

    return _aqi_get_row(aqi)[PM25_IDX_COLOR]


def aqi_to_label(aqi: int) -> str:
    """
    Return a label for an Air Quality Index.

    Parameters:
    aqi -- Air Quality Index for which to return a label.

    Result:
    Any of Good/Moderate/USG/Unhealthy/Very Unhealthy/Hazardous.
    """

    return _aqi_get_row(aqi)[PM25_IDX_LABEL]
