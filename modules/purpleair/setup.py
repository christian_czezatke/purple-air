#!/usr/bin/env python3

from setuptools import setup

setup(name='purpleair',
      version='0.1',
      description='Python API to query Purpleair air quality sensors.',
      url='https://bitbucket.org/christian_czezatke/purpleair/src/master/modules/purpleair',
      author='Christian Czezatke',
      author_email='purpleair@ceci.at',
      license='BSD',
      packages=['purpleair'],
      install_requires = [ 'requests' ],
      zip_safe=False)
