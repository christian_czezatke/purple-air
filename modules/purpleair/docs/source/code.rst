Documentation for the Code
**************************

Purpleair Classes
-----------------

Classes for locating Purpleair sensors based on different criteria and
for retrieving current and historic data.

.. automodule:: purpleair.sensors
   :members:

AQI
---

Helper functions for converting sensor readings to the EPA Air Quality Index rating.

.. automodule:: purpleair.aqi
   :members:

      
