.. Purple Air Library documentation master file, created by
   sphinx-quickstart on Mon Dec 31 15:33:13 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Purple Air Library's documentation!
==============================================

This library is used to locate Purpleair air quality sensors and to retrieve
current and historic data for a sensor.

The library also contains some helper functions to convert 2.5ug/m^3
particular readings into the EPA's Air Quality Index (AQI).

To learn more about Purpleair, visit http://www.purpleair.com

The latest version of this library can be found `here <https://bitbucket.org/christian_czezatke/purple-air/downloads/>`_. (This repo also contains an app that can be used as an example).

Disclaimer: Besides owning a Purpleair sensor, I have no affiliation with the company.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
