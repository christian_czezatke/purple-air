#!/usr/bin/env python3

from geopy.geocoders import Nominatim
from geopy import distance

import json
import requests
import os
import time
import datetime

import dash
import dash_core_components as dcc
import dash_html_components as html

import dateutil.parser
from dateutil import tz
from datetime import datetime
from purpleair.aqi import pm25_to_aqi, aqi_to_color, aqi_to_label
import purpleair.sensors as sensors

# Convert between miles and kilometers
KM_PER_MILE = 1.609344

GRAPH_PROPERTIES = [
    {"label": "PM2.5 AQI", "value": "AQI"},
    {"label": "PM1.0", "value": "PM1.0"},
    {"label": "PM2.5", "value": "PM2.5"},
    {"label": "PM10", "value": "PM10"},
    {"label": "Temperature", "value": "TEMPERATURE"},
    {"label": "Humidity", "value": "HUMIDITY"},
    {"label": "Pressure", "value": "PRESSURE"},
]


def get_closest_purple(latitude, longitude, count=1, max_distance=1000000):

    directory = sensors.Directory()
    filter_func = lambda x: \
        sensors.Directory.recently_reporting(x) and sensors.Directory.outdoors(x)
    distance_filter = lambda x: \
        sensors.Directory.close_by(x, latitude, longitude, max_distance)

    stations = directory.get_sensors(filter_func=filter_func,
                                     sort_filter_func=distance_filter,
                                     max_result=count)

    return {
        "location" : { "latitude": latitude,
                       "longitude": longitude
                       },
        "stations" : stations,
        }


def get_station_data(context):
    latitude = context["location"]["latitude"]
    longitude = context["location"]["longitude"]

    result = []
    for station in context["stations"]:
        sd = station.get_live_data()
        result.append([distance.distance((latitude, longitude),
                                         station.location), sd])
    return result


def get_my_location(approx_string):
    geolocator = Nominatim(user_agent="test")
    location = geolocator.geocode(approx_string)
    return location


# The first is the standard Dash CSS.
# The second is a css that defines a style for _dash-loading-callback, to grey out the page
# while a callback is running

external_stylesheets = [#'https://codepen.io/chriddyp/pen/bWLwgP.css',
                        #'https://codepen.io/chriddyp/pen/brPBPO.css'
]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = "Air Quality Monitor"

app.layout = html.Div(children=[
    html.H1(children='Air Quality'),
    html.Div(children= [
        html.Label("Find up to", style={"whiteSpace":"nowrap"}),
        html.Div(style={"width": "10px"}),
        dcc.Dropdown(id="dropdown-stationcount",
                     options = [{ "label": str(x), "value": str(x) }
                                for x in range(1,11)],
                      value="5",
                     clearable=False),
        html.Div(style={"width": "10px"}),
        html.Label("station(s) around", style={"whiteSpace":"nowrap"}),
        html.Div(style={"width": "10px"}),
        dcc.Input(id="location-text", type="text"),
        html.Div(style={"width": "10px"}),
        html.Label("up to", style={"whiteSpace":"nowrap"}),
        html.Div(style={"width": "10px"}),
        dcc.Input(id="location-distance", type="numeric",
                  min=1, max=500, size=1,
                  value=100),
        html.Div(style={"width": "10px"}),
        dcc.Dropdown(id="dropdown-distance-units",
                     options = [ { "label": "mi", "value": "mi" },
                                 { "label": "km", "value": "km" } ],
                     value="mi",
                     clearable=False),
        html.Div(style={"width": "10px"}),
        html.Label("away."),
        html.Div(style={"width": "10px"}),
        html.Button("Go", id="location-button")],
             style={'display': 'flex', "flexDirection": "row",
                    "justifyContent": "left",
                    "alignItems": "center",
                    "paddingBottom": "20px"}),


#    create_location_wrapper(),

    html.Div(id="location-loading"),
    html.Div(id="location-output",
             children=[#html.Div(children="No location"),
                       html.Div(id="state", style={"display": "none"})]),

    html.Div(children =
              dcc.Tabs(id="data-tab", value="aqi-table", children = [
                  dcc.Tab(label="Overview", value="aqi-table",
                          children=[html.Div(id="aqi-output-loading"),
                                    html.Div(id="aqi-output")]),
                  dcc.Tab(label="Station Graph", value="graph",
                          children=[html.Div(children=[
                              html.Label("Station:"),
                              html.Div(style={"width": "10px"}),
                              dcc.Dropdown(id="station-dropdown", style={"width":"350px"}),
                              html.Div(style={"width": "30px"}),
                              html.Label("Property:"),
                              html.Div(style={"width": "10px"}),
                              dcc.Dropdown(id="datatype-dropdown", options =
                                           GRAPH_PROPERTIES,
                                           value="AQI",
                                           clearable=False,
                                           style={"width":"120px"})],
                                    style={'display': 'flex',
                                           "flexDirection": "row",
                                           "justifyContent": "left",
                                           "alignItems": "center",
                                           "paddingTop": "20px"}), # end of Div

                              html.Div(id="station-graph-loading"),
                              html.Div(id="station-graph",
                                       children=[dcc.Graph(id="station-graph-graph")])])]),
              style={"align": "center", "transtion": "position 2s"}),


    #html.Div(id="aqi-output"),


    html.Div(id="dummy"),


    dcc.Interval(id="aqi-interval", interval = 10 * 1000, n_intervals=0),




    # dcc.Graph(
    #     id='example-graph',
    #     figure={
    #         'data': [
    #             {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
    #             {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
    #         ],
    #         'layout': {
    #             'title': 'Dash Data Visualization'
    #         }
    #     }
    # )
])


def render_station(air_data):
    dist = air_data[0]
    data = air_data[1]
    ###print (data)
    timeline = json.loads(data["Stats"])
    
    aqi = []
    for x in [ "v", "v1", "v2", "v3", "v4", "v5", "v6"]:
        aqi.append(pm25_to_aqi(timeline[x]))

    return [
        html.Tr(
            [html.Td(children=[html.Div(data["Label"]),
                               html.Div("{0}mi/{1}km"\
                                        .format(round(dist.miles, 1), round(dist.km, 1)))])] +
            [html.Td(children=[html.Div(x), html.Div(aqi_to_label(x))],
                               style={"width": "80px", "textAlign": "center", "backgroundColor": aqi_to_color(x)}) for x in aqi] +
            [html.Td(children=[html.Div("{0}F/{1}%".format(data["temp_f"], data["humidity"])),
                               html.Div("{0} mbar".format(data["pressure"]))],
                     style={"textAlign": "center"})]

        )
    ]

def render_aqi_table(air_data):

    headers = [html.Th("Name", style={"textAlign": "left"})] + \
              [html.Th(x, style={"textAlign": "center"})
               for x in ["Now", "10min", "30min", "1hr", "6hr", "24hr", "1wk", "T/H/P"]]

    table_rows = [html.Tr(headers)]
    for station in air_data:
        table_rows.extend(render_station(station))

    tbl = html.Table(table_rows)

    now = datetime.now()
    now_str = now.strftime("%b %e %Y, %H:%M:%S")

    return [
        html.Div(children=[
            html.Div(children=tbl),
            html.Div(children="Latest update: {0}".format(now_str))
        ])
    ]


def render_location(location):
    return [
        html.H3(children=location.address),
        #html.Div("({0}/{1})".format(location.longitude, location.latitude)),
    ]

        
def make_loading_indicator(text, id):
    return html.Div([html.H3(text, style={"paddingTop": "20px",
                                          "paddindBottom": "20px",
                                          "textAlign": "center"})], id=id)


@app.callback(
    dash.dependencies.Output("location-loading", "children"),
    [dash.dependencies.Input("location-button", "n_clicks")]
    )
def loading_location(n_clicks):
    if n_clicks:
        return html.Div([dcc.Markdown(
                         '''Resolving Location ...''')], id='location-output')

@app.callback(
    dash.dependencies.Output("location-output", "children"),
    #dash.dependencies.Output("state-location-wrapper", "children"),
    [dash.dependencies.Input("location-button", "n_clicks")],
    [dash.dependencies.State("location-text", "value"),
     dash.dependencies.State("dropdown-stationcount", "value"),
     dash.dependencies.State("location-distance", "value"),
     dash.dependencies.State("dropdown-distance-units", "value"),
    ])
def update_location(n_clicks, location_name, station_count, max_distance, distance_units):
    location_data = None
    location_div = []

    print("Update location")

    if location_name:
        location = get_my_location(location_name)
        if location is not None:

            max_distance = float(max_distance)
            if distance_units == "mi":
                # Internally we work on kilometers. Convert miles.
                max_distance *= KM_PER_MILE
            location_data = get_closest_purple(location.latitude, location.longitude,
                                               int(station_count), max_distance)
            location_div = render_location(location)
            location_data["stations"] = [x.to_dict() for x in location_data["stations"]]
        else:
            print("Unknown location")
            location_div = [html.Div("*** Unknown location ***")]

           
    return location_div + [html.Div(id="state", children=json.dumps(location_data), style={"display": "none"})]

@app.callback(
    dash.dependencies.Output("aqi-output-loading", "children"),
    [dash.dependencies.Input("state", "children")])
def update_air_quality_loading(state):
    if state and state != "null":
        return make_loading_indicator("Loading Air Quality Data ...", "aqi-output")
    else:
        # Prevent flicker on startup.
        print("NO STATE SET")
        return ""


@app.callback(
    dash.dependencies.Output("aqi-output", "children"),
    [dash.dependencies.Input("aqi-interval", "n_intervals"),
     dash.dependencies.Input("state", "children")])
def update_air_quality(n_intervals, children):
    print("Update air quality")

    if not children:
        return ""

    try:
        location_data = json.loads(children)
        if not location_data:
            return ""
    except Exception as e:
        print("Cannot jsonify location data.")
        return ""
    try:
        location_data["stations"] = [sensors.SensorHandle(x) for x in location_data["stations"]]
        air_data = get_station_data(location_data)
        if len(air_data) > 0:
            result = render_aqi_table(air_data)
        else:
            result = html.H3("No stations found")
    except Exception as e:
        print(e)
        result = ""
    return result


@app.callback(
     dash.dependencies.Output("station-dropdown", "options"),
     [dash.dependencies.Input("state", "children")]
     )
def update_dropdown(state):
    opts = []
    location_data = None

    print("Update Dropdown")

    if state:
        location_data = json.loads(state)

    if location_data:
        location_data["stations"] = [sensors.SensorHandle(x) for x in location_data["stations"]]
        for station in location_data["stations"]:           
            opts.append({ "label": station.label, "value": station.channels["CH_A"].sensor_id})

    return opts


def to_local_timestr(tw_timestr, target_tz):
    return dateutil.parser.parse(tw_timestr).astimezone(target_tz).isoformat()


@app.callback(
    dash.dependencies.Output("station-graph-loading", "children"),
    [dash.dependencies.Input("station-dropdown", "value"),
     dash.dependencies.Input("station-dropdown", "options"),
     dash.dependencies.Input("datatype-dropdown", "value")]
    )
def update_graph_loading(station_id, options, datatype_key):
    if station_id not in [x["value"] for x in options]:
        # A new location was selected and the graph is not among the
        # current list of stations. -- Clear the area.
        return ""
    else:
        return make_loading_indicator("Loading Graph ...", "station-graph")
    

@app.callback(
    dash.dependencies.Output("station-graph", "children"),
    [dash.dependencies.Input("station-dropdown", "value"),
     dash.dependencies.Input("datatype-dropdown", "value")],
    [dash.dependencies.State("state", "children")])
def update_graph(station_id, datatype_key, children):
    #print("Update graph: {0} {1}".format(value, children))

    if not children:
        return ""

    location_data = json.loads(children)

    if not location_data:
        return ""

    station = None

    # Find the selected station in the list of stations:
    location_data["stations"] = [sensors.SensorHandle(x) for x in location_data["stations"]]
    for s in location_data["stations"]:
        if s.channels["CH_A"].sensor_id == station_id:
            station = s
            break

    if station is None:
        print("STATION IS NONE")
        return ""
    
    if datatype_key == "AQI":
        ts_key = "PM2.5"
    else:
        ts_key = datatype_key
    
    labels, numbers = station.get_historic_data(ts_key)

    data_name = [x["label"] for x in GRAPH_PROPERTIES if x["value"] == datatype_key][0]

    if labels is None:
        data_name = data_name +": Data not available."
        colors = None
        data_mode = None
    else:
    
        if datatype_key == "AQI":
            numbers = [pm25_to_aqi(x) for x in numbers]
            colors = [aqi_to_color(x) for x in numbers]

        else:
            colors=["blue"]*len(numbers)

        if datatype_key in [ "HUMIDITY", "TEMPERATURE", "PRESSSURE" ]:
            # Humidity, Temperature, Pressure: -- Line chart
            data_mode=None
        else:
            data_mode="markers"

    return dcc.Graph(
        id='station-graph-graph',
          figure={
              'data': [
                  {'x': labels, 'y': numbers,
                   'type':"line", "marker": {"color":colors},
                   "mode":data_mode},

              ],
              'layout': {
                  'title': '{0} - {1}'.format(station.label, data_name),
              },
         },
        #animate=True #-- unfortunately this breaks auto-scaling.
     )


if __name__ == '__main__':
    app.run_server(debug=True, host="0.0.0.0")
